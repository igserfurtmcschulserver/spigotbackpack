package de.cag_igs.spigotbackpack.commands;

import de.cag_igs.spigotbackpack.items.BaseBackpack;
import de.cag_igs.spigotbackpack.items.BigBackpack;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Markus on 09.04.2017.
 */
public class GetBigBackpackCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("getbigbackpack")) {
            if (sender.hasPermission("spigotbackpack.getbigbackpack")) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    BaseBackpack backpack = null;
                    if (args.length == 1) {
                        backpack = new BigBackpack(UUID.fromString(args[0]));
                    } else {
                        backpack = new BigBackpack();
                    }


                    ItemStack item = backpack.getItemStack();

                    for (int i = 0; i < player.getInventory().getSize(); ++i) {
                        if (player.getInventory().getItem(i) == null) {
                            player.getInventory().setItem(i, item);
                            return true;
                        }
                    }
                    player.sendMessage("Du hattest keinen Platz im Inventar!");
                    return true;
                } else {
                    sender.sendMessage("Only players can get a backpack!");
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }
}
