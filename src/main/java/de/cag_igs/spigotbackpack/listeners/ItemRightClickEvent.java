package de.cag_igs.spigotbackpack.listeners;


import de.cag_igs.spigotbackpack.items.BaseBackpack;
import de.cag_igs.spigotbackpack.items.BigBackpack;
import de.cag_igs.spigotbackpack.items.MediumBackpack;
import de.cag_igs.spigotbackpack.items.SmallBackpack;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class ItemRightClickEvent implements Listener {
    @EventHandler
    public void onItemRightClickEvent(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            ItemStack item = event.getItem();
            if (item != null) {
                NBTItem nbtItem = new NBTItem(item);
                BaseBackpack backpack = null;
                if (nbtItem.hasKey("sb_type")) {
                    if (nbtItem.getString("sb_type").equalsIgnoreCase("smallbackpack")) {
                        if (!nbtItem.hasKey("sb_itemuuid")) {
                            backpack = new SmallBackpack();
                            replaceItemInPlayerInventory(item, backpack.getItemStack(), event.getPlayer().getInventory());
                        } else {
                            backpack = new SmallBackpack(item);
                        }
                    } else if (nbtItem.getString("sb_type").equalsIgnoreCase("mediumbackpack")) {
                        if (!nbtItem.hasKey("sb_itemuuid")) {
                            backpack = new MediumBackpack();
                            replaceItemInPlayerInventory(item, backpack.getItemStack(), event.getPlayer().getInventory());
                        } else {
                            backpack = new MediumBackpack(item);
                        }
                    } else if (nbtItem.getString("sb_type").equalsIgnoreCase("bigbackpack")) {
                        if (!nbtItem.hasKey("sb_itemuuid")) {
                            backpack = new BigBackpack();
                            replaceItemInPlayerInventory(item, backpack.getItemStack(), event.getPlayer().getInventory());
                        } else {
                            backpack = new BigBackpack(item);
                        }
                    }
                }
                if (backpack != null) {
                    event.getPlayer().openInventory(backpack.getInventory());
                }
            }
        }
    }

    private void replaceItemInPlayerInventory(ItemStack oldItem, ItemStack newItem, PlayerInventory inventory) {
        for (int i = 0; i < inventory.getSize(); ++i) {
            if (inventory.getItem(i) != null && inventory.getItem(i).equals(oldItem)) {
                inventory.clear(i);
                inventory.setItem(i, newItem);
                break;
            }
        }
    }
}
