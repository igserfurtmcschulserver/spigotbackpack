package de.cag_igs.spigotbackpack.listeners;

import de.cag_igs.spigotbackpack.data.InventorySerialize;
import de.cag_igs.spigotbackpack.items.BaseBackpack;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class InventoryCloseEvent implements Listener {

    @EventHandler
    public void onInventoryCloseEvent(org.bukkit.event.inventory.InventoryCloseEvent event) {
        if (event.getInventory().getHolder() instanceof BaseBackpack) {
            BaseBackpack backpack = (BaseBackpack) event.getInventory().getHolder();
            backpack.saveInventory();
        }
    }
}
