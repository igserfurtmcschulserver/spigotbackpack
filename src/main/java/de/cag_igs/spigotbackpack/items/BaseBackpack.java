package de.cag_igs.spigotbackpack.items;

import de.cag_igs.spigotbackpack.data.DataManager;
import de.cag_igs.spigotbackpack.data.InventorySerialize;
import de.cag_igs.spigotbackpack.data.database.DatabaseFactory;
import de.tr7zw.itemnbtapi.ItemNBTAPI;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;


public abstract class BaseBackpack implements InventoryHolder {
    protected NBTItem item;

    protected Inventory inventory = null;

    protected UUID uuid;

    protected InventoryType inventoryType = null;

    protected int inventorySize = 0;

    protected String displayName;

    public BaseBackpack() {

    }

    public BaseBackpack(ItemStack itemStack) {
        item = ItemNBTAPI.getNBTItem(itemStack);
        if (item.hasKey("itemuuid")) {
            uuid = item.getObject("itemuuid", UUID.class);
        } else {
            uuid = null;
        }
        if (item.hasKey("specialtype") && item.getString("specialtype").equalsIgnoreCase("backpack")) {
            inventory = InventorySerialize.deserializeInventory(DataManager.getContent(uuid), this);
        }
    }

    public ItemStack getItemStack() {
        return item.getItem();
    }

    public InventoryType getInventoryType() {
        return inventoryType;
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    public void saveInventory() {
        String serializedInventory = InventorySerialize.serializeInventory(inventory);
        DataManager.updateContent(uuid, serializedInventory);
    }

    public int getInventorySize() {
        return inventorySize;
    }
}
