package de.cag_igs.spigotbackpack.items;

import de.cag_igs.spigotbackpack.data.DataManager;
import de.cag_igs.spigotbackpack.data.InventorySerialize;
import de.tr7zw.itemnbtapi.ItemNBTAPI;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

/**
 * Created by Markus on 09.04.2017.
 */
public class BigBackpack extends BaseBackpack {
    public BigBackpack() {
        ItemStack itemStack = new ItemStack(Material.RABBIT_HIDE, 1);
        displayName = "Grosser Rucksack";
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(displayName);
        itemStack.setItemMeta(meta);
        item = ItemNBTAPI.getNBTItem(itemStack);
        uuid = UUID.randomUUID();
        item.setObject("sb_itemuuid", uuid);
        item.setString("sb_type", "bigbackpack");
        //item.setString("sb_displayname", displayName);
        inventorySize = 27;
        inventory = Bukkit.createInventory(this, inventorySize, displayName);
        inventoryType = inventory.getType();
        DataManager.updateContent(uuid, InventorySerialize.serializeInventory(inventory));
    }

    public BigBackpack(ItemStack itemStack) {
        item = ItemNBTAPI.getNBTItem(itemStack);
        if (item.hasKey("sb_itemuuid")) {
            uuid = item.getObject("sb_itemuuid", UUID.class);
        } else {
            uuid = null;
        }
        if (item.hasKey("sb_type") && item.getString("sb_type").equalsIgnoreCase("bigbackpack")) {
            inventory = InventorySerialize.deserializeInventory(DataManager.getContent(uuid), this);
            inventoryType = inventory.getType();
            inventorySize = inventory.getSize();
            displayName = inventory.getName();
        }
    }

    public BigBackpack(UUID uuid) {
        ItemStack itemStack = new ItemStack(Material.RABBIT_HIDE, 1);
        displayName = "Grosser Rucksack";
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(displayName);
        itemStack.setItemMeta(meta);
        item = ItemNBTAPI.getNBTItem(itemStack);
        this.uuid = uuid;
        item.setObject("sb_itemuuid", uuid);
        item.setString("sb_type", "bigbackpack");
        //item.setString("sb_displayname", displayName);
        inventorySize = 27;
        inventory = Bukkit.createInventory(this, inventorySize, displayName);
        inventoryType = inventory.getType();
        DataManager.updateContent(uuid, InventorySerialize.serializeInventory(inventory));
    }

    public static ItemStack getBigDummyBackpack() {
        ItemStack itemStack = new ItemStack(Material.RABBIT_HIDE, 1);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName("Grosser Rucksack");
        itemStack.setItemMeta(meta);
        NBTItem item = ItemNBTAPI.getNBTItem(itemStack);
        item.setString("sb_type", "bigbackpack");
        return item.getItem();
    }
}
