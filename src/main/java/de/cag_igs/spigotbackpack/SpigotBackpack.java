package de.cag_igs.spigotbackpack;

import de.cag_igs.spigotbackpack.commands.GetBigBackpackCommand;
import de.cag_igs.spigotbackpack.commands.GetMediumBackpackCommand;
import de.cag_igs.spigotbackpack.commands.GetSmallBackpackCommand;
import de.cag_igs.spigotbackpack.data.DataManager;
import de.cag_igs.spigotbackpack.data.database.DatabaseFactory;
import de.cag_igs.spigotbackpack.items.BigBackpack;
import de.cag_igs.spigotbackpack.items.MediumBackpack;
import de.cag_igs.spigotbackpack.items.SmallBackpack;
import de.cag_igs.spigotbackpack.listeners.InventoryCloseEvent;
import de.cag_igs.spigotbackpack.listeners.ItemRightClickEvent;
import org.bukkit.Material;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotBackpack extends JavaPlugin {

    @Override
    public void onDisable() {
        DatabaseFactory.getDatabaseObject().close();
        super.onDisable();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        this.loadConfig();
        DatabaseFactory.createDatabaseObject(this);
        DataManager.createTables();
        this.getCommand("getsmallbackpack").setExecutor(new GetSmallBackpackCommand());
        this.getCommand("getmediumbackpack").setExecutor(new GetMediumBackpackCommand());
        this.getCommand("getbigbackpack").setExecutor(new GetBigBackpackCommand());
        this.getServer().getPluginManager().registerEvents(new ItemRightClickEvent(), this);
        this.getServer().getPluginManager().registerEvents(new InventoryCloseEvent(), this);
        addRecipes();
    }

    public void loadConfig() {
        this.reloadConfig();
        this.getConfig().options().header("Configuration for Spigotbackpacks!");
        this.getConfig().addDefault("datastore.type", "mysql");
        this.getConfig().addDefault("crafting.smallbackpack", true);
        this.getConfig().addDefault("crafting.mediumbackpack", true);
        this.getConfig().addDefault("crafting.bigbackpack", true);
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }

    public void addRecipes() {
        if (this.getConfig().getBoolean("crafting.smallbackpack")) {
            ShapedRecipe smallBackpackRecipe = new ShapedRecipe(SmallBackpack.getSmallDummyBackpack());
            smallBackpackRecipe.shape("sls", "lcl", "shs");
            smallBackpackRecipe.setIngredient('s', Material.STRING);
            smallBackpackRecipe.setIngredient('l', Material.LEATHER);
            smallBackpackRecipe.setIngredient('c', Material.CHEST);
            smallBackpackRecipe.setIngredient('h', Material.TRIPWIRE_HOOK);
            this.getServer().addRecipe(smallBackpackRecipe);
        }

        if (this.getConfig().getBoolean("crafting.mediumbackpack")) {
            ShapedRecipe mediumBackpackRecipe = new ShapedRecipe(MediumBackpack.getMediumDummyBackpack());
            mediumBackpackRecipe.shape("shs", "lcl", "iri");
            mediumBackpackRecipe.setIngredient('s', Material.STRING);
            mediumBackpackRecipe.setIngredient('h', Material.TRIPWIRE_HOOK);
            mediumBackpackRecipe.setIngredient('l', Material.LEATHER);
            mediumBackpackRecipe.setIngredient('c', Material.CHEST);
            mediumBackpackRecipe.setIngredient('i', Material.IRON_INGOT);
            mediumBackpackRecipe.setIngredient('r', Material.RABBIT_HIDE);
            this.getServer().addRecipe(mediumBackpackRecipe);
        }

        if (this.getConfig().getBoolean("crafting.bigbackpack")) {
            ShapedRecipe bigBackpackRecipe = new ShapedRecipe(BigBackpack.getBigDummyBackpack());
            bigBackpackRecipe.shape("lbl", "ici", "shs");
            bigBackpackRecipe.setIngredient('l', Material.LEATHER);
            bigBackpackRecipe.setIngredient('b', Material.SHULKER_SHELL);
            bigBackpackRecipe.setIngredient('i', Material.IRON_INGOT);
            bigBackpackRecipe.setIngredient('c', Material.CHEST);
            bigBackpackRecipe.setIngredient('s', Material.STRING);
            bigBackpackRecipe.setIngredient('h', Material.TRIPWIRE_HOOK);
            this.getServer().addRecipe(bigBackpackRecipe);
        }
    }
}
