package de.cag_igs.spigotbackpack.data;

import de.cag_igs.spigotbackpack.data.database.DatabaseFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class DataManager {

    public static void createTables() {
        DatabaseFactory.getDatabaseObject().doUpdate("CREATE TABLE IF NOT EXISTS `spigotbackpack_backpacks` " +
                "(itemuuid VARCHAR(36) NOT NULL PRIMARY KEY, backpackcontent TEXT)");
    }

    public static void updateContent(UUID uuid, String content) {
        DatabaseFactory.getDatabaseObject().doSafeUpdate("INSERT INTO `spigotbackpack_backpacks` (itemuuid, " +
                "backpackcontent) VALUES (?, ?) ON DUPLICATE KEY UPDATE `backpackcontent`=?", uuid.toString(),
                content, content);
    }

    public static String getContent(UUID uuid) {
        ResultSet results = DatabaseFactory.getDatabaseObject().doSafeQuery("SELECT backpackcontent FROM " +
                "`spigotbackpack_backpacks` WHERE `itemuuid`=?", uuid.toString());
        try {
            String temp = "null";
            while (results.next()) {
                temp = results.getString("backpackcontent");
            }
            return temp;
        } catch (SQLException e) {
            e.printStackTrace();
            return "null";
        }
    }
}
