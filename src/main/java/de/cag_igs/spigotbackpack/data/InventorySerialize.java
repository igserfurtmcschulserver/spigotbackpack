package de.cag_igs.spigotbackpack.data;

import com.google.common.io.BaseEncoding;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;



public class InventorySerialize {


    public static String serializeInventory(Inventory inventory) {
        if (inventory == null) {
            return "null";
        }
        YamlConfiguration configuration = new YamlConfiguration();
        configuration.set("size", inventory.getSize());
        configuration.set("name", inventory.getName());

        for (int i = 0; i < inventory.getSize(); ++i) {
            configuration.set("" + i, inventory.getContents()[i]);
        }
        return configuration.saveToString();
    }

    public static Inventory deserializeInventory(String serializedInventory, InventoryHolder owner) {
        if (serializedInventory.equals("null") || serializedInventory == null || serializedInventory.isEmpty()) {
            return null;
        }
        YamlConfiguration configuration = new YamlConfiguration();
        try {
            configuration.loadFromString(serializedInventory);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return null;
        }
        Inventory inventory = Bukkit.createInventory(owner, configuration.getInt("size"), configuration.getString
                ("name"));

        for (int i = 0; i < inventory.getSize(); ++i) {
            inventory.setItem(i, configuration.getItemStack("" + i));
        }
        return inventory;
    }
}
