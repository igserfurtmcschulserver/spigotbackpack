package de.cag_igs.spigotbackpack.data.database;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.*;

public class MySql implements IDatabase {
    private Connection connection;
    private String pluginName;
    private String host;
    private int port;
    private String database;
    private String user;
    private String password;

    public MySql(String pluginName) {
        this.pluginName = pluginName;
        configuration();
    }

    private void configuration() {
        File file = new File("plugins" + File.separator + pluginName + File.separator + "mysqldata.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        cfg.addDefault("mysql.hostname", "localhost");
        cfg.addDefault("mysql.port", 3306);
        cfg.addDefault("mysql.database", "defaultdatabase");
        cfg.addDefault("mysql.user", "root");
        cfg.addDefault("mysql.password", "toor");
        cfg.options().copyDefaults(true);

        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.host = cfg.getString("mysql.hostname");
        this.port = cfg.getInt("mysql.port");
        this.database = cfg.getString("mysql.database");
        this.user = cfg.getString("mysql.user");
        this.password = cfg.getString("mysql.password");
    }

    @Override
    public boolean connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try {
                this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database,
                        user, password);
                if (!this.connection.isClosed()) {
                    return true;
                }
                return false;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isConnected() {
        try {
            if (connection == null) {
                return false;
            }
            return !connection.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void close() {
        try {
            if (this.connection != null && !connection.isClosed()) {
                this.connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean doUpdate(String sql) {
        if (!this.isConnected()) {
            this.connect();
        }
        try {
            this.connection.createStatement().executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean doSafeUpdate(String sql, String... args) {
        if (!this.isConnected()) {
            this.connect();
        }
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        for (int i = 0; i < args.length; ++i) {
            try {
                preparedStatement.setString(i + 1, args[i]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ResultSet doQuery(String sql) {
        if (!this.isConnected()) {
            this.connect();
        }
        try {
            return this.connection.createStatement().executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet doSafeQuery(String sql, String... args) {
        if (!this.isConnected()) {
            this.connect();
        }
        ResultSet results = null;
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        for (int i = 0; i < args.length; ++i) {
            try {
                preparedStatement.setString(i + 1, args[i]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            results = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }
}
