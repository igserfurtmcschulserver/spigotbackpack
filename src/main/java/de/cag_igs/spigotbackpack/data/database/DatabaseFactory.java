package de.cag_igs.spigotbackpack.data.database;


import org.bukkit.plugin.java.JavaPlugin;

public class DatabaseFactory {
    private static IDatabase databaseObject = null;

    public static void createDatabaseObject(JavaPlugin plugin) {
        if (plugin.getConfig().getString("datastore.type").equalsIgnoreCase("mysql") && databaseObject == null) {
            databaseObject = new MySql(plugin.getName());
        }
    }

    public static IDatabase getDatabaseObject() {
        return databaseObject;
    }
}
