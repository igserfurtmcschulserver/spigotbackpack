package de.cag_igs.spigotbackpack.data.database;

import java.sql.ResultSet;

public interface IDatabase {
    boolean connect();

    boolean isConnected();

    void close();

    boolean doUpdate(String sql);

    boolean doSafeUpdate(String sql, String... args);

    ResultSet doQuery(String sql);

    ResultSet doSafeQuery(String sql, String... args);
}
